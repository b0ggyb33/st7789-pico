project(test-pico C CXX ASM)

find_package(Catch2)

include_directories(../lib)

add_executable(test-pico tests.cpp)
target_link_libraries(test-pico PRIVATE Catch2::Catch2)

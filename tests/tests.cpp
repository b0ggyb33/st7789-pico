#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>


#include "rp2040.hpp"

class FakeRP2040: public RP2040_Interface
{
public:

  FakeRP2040()
  {
    for (int i=0;i<40;++i) enabled[i]=false;
  }

   void set_dir(uint8_t pin, bool direction) override
   {
     if (enabled[pin]) directions[pin] = direction;
     else throw std::exception();
   }

   bool is_dir_out(uint8_t pin) override
   {
     return directions[pin];
   }
   
   void init(uint8_t pin) override
   {
     directions[pin]=false;
     values[pin] = false;
     enabled[pin]=true;
   }


   void put(uint8_t pin, bool value) override {values[pin]=value;}
   bool get(uint8_t pin) override {return values[pin];}
   void sleep(uint8_t) override {}


private:
   bool directions[40];
   bool values[40];
   bool enabled[40];
};

TEST_CASE("Can set direction of GPIO pin", "[gpio]"){
  std::shared_ptr<RP2040_Interface> f = std::make_shared<FakeRP2040>(FakeRP2040());
  ST7789 st7789 = ST7789(f);
  f->init(0);
  st7789.set_output(0);
  REQUIRE(st7789.is_output(0));
  f->init(1);
  st7789.set_output(1);
  REQUIRE(st7789.is_output(1));
}

TEST_CASE("Can turn on GPIO pin", "[gpio]"){
  std::shared_ptr<RP2040_Interface> fake = std::make_shared<FakeRP2040>(FakeRP2040());
  ST7789 st7789 = ST7789(fake);
  fake->init(0);
  st7789.set_output(0);
  st7789.turn_on(0);
  REQUIRE(st7789.is_on(0));

  fake->init(1);
  st7789.set_output(1);
  REQUIRE(!st7789.is_on(1));
}

TEST_CASE("Can turn off GPIO pin", "[gpio]"){

  std::shared_ptr<RP2040_Interface> fake =std::static_pointer_cast<RP2040_Interface>(std::make_shared<FakeRP2040>());
  ST7789 st7789 = ST7789(fake);
  fake->init(0);
  st7789.set_output(0);
  st7789.turn_on(0);
  st7789.turn_off(0);
  REQUIRE(!st7789.is_on(0));
}

TEST_CASE("pin 25 is initialised", "[gpio]"){
  std::shared_ptr<RP2040_Interface> fake =std::static_pointer_cast<RP2040_Interface>(std::make_shared<FakeRP2040>());
  ST7789 st7789 = ST7789(fake);
  REQUIRE(st7789.is_output(25));

}

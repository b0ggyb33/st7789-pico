#include <stdio.h>
#include "pico/stdlib.h"
#include "st7789/st7789.hpp"
#include <memory>

#define RED 0xF800
#define GREEN 0x07E0
#define BLUE 0x001F
#define WHITE 0xFFFF
#define BLACK 0x0000
int main()
{
    stdio_init_all();

    uint16_t image[240*240];

    uint16_t width = 240;
    uint16_t height = 240;
    pimoroni::ST7789 s = pimoroni::ST7789(width, height, (uint16_t*)&image);
    s.init();

    uint16_t colours[] = {BLACK, RED, GREEN, BLUE, WHITE};
  
    uint8_t n=0;
    while(true)
    {
      uint16_t colour = colours[n];
      printf("%d:", n);
      for (int i=0;i<240*240;++i) image[i] = colour;
      printf("%x\n", colour);
      s.update();
      n+=1;
      n%=5;
      sleep_ms(1000);
    }

    return 0;
}
